; Полуянов Александр Михайлович P32141

section .text

; Принимает код возврата и завершает текущий процесс
exit: ; rdi - exit code
    mov rax, 60 ; 'exit' syscall
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte[rax+rdi],0 ; null-terminator check
        je .return
        inc rax ; len++
        jmp .loop
    .return:
	    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    push rdi
    call string_length
    pop rdi
    mov rdx, rax ; string length in bytes
    mov rsi, rdi
    mov rdi, 1 ; stdout descriptor
    mov rax, 1 ; 'write' syscall number
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi
    mov rsi, rsp ; string address
    pop rdi  
    mov rdx, 1 ; string length in bytes
    mov rdi, 1 ; stdout descriptor
    mov rax, 1 ; 'write' syscall number
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, '\n' ; \n и 0xA эквивалентны, но для улучшения читабельности исправил
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint: 
    mov r8, 10
    mov rax, rdi
    mov rdi, rsp
    dec rdi
    push 0
    sub rsp, 20
    .loop:
        xor rdx, rdx
        div r8
        add rdx, '0'
        dec rdi
        mov [rdi], dl
        cmp rax, 0
        jnz .loop
    call print_string
    add rsp, 28
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    xor rax, rax
    cmp rdi, 0
    jns .pos
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    .pos:
	    jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    .loop:
        mov dl, [rsi + rcx]
        cmp dl, [rdi + rcx]
        jne .error
        inc rcx
        cmp dl, 0
        jne .loop
        mov rax, 1
        jmp .return
    .error:
	    xor rax, rax
    .return:
	    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push rdx
    mov rdx, 1
    xor rdi, rdi
    xor rax, rax
    push 0
    mov rsi, rsp
    syscall
    pop rax
    pop rdx
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    mov r8, rdi
    mov r9, rsi
    xor r10, r10
    .skip:
        call read_char
        cmp rax, `\t` ; скип таба
        je .skip
        cmp rax, `\n` ; скип переноса
        je .skip
        cmp rax, ` ` ; скип пробела
        je .skip
    .loop:
        cmp rax, ` `
        je .end
        cmp rax, `\t`
        je .end
        cmp rax, `\n`
        je .end
        cmp rax, 0
        je .end
        cmp r9, r10
        je .out
        mov byte[r8 + r10], al
        inc r10
        call read_char
        jmp .loop
    .end:
        xor al, al
        mov byte[r8 + r10], al
        mov rax, r8
        mov rdx, r10
        ret
    .out:
        xor rax, rax
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor r8, r8
    .loop:
        mov r8b, [rdi + rdx]
        cmp r8b, '0'
        jb .return
        cmp r8b, '9'
        ja .return
        inc rdx
        sub r8b, '0'
        imul rax, 10
        add rax, r8
        jmp .loop
    .return:
	    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
    inc rdx
    neg rax
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy: 
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    xor r10, r10
    mov r8, rax 
    cmp r8, rdx
    jge .out
    xor rax, rax
    .loop:
        mov al, byte[rdi + r10]
        mov byte[rsi + r10], al
        inc r10
        cmp al, 0
        jne .loop
        mov rax, r10
        ret
    .out:
        xor rax, rax
        ret
